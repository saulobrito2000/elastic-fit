<?php
require_once 'Trackback.php';

class DataProcessing {
	private $data;
	protected $getDataJson;

    public function convertToJson($dataHtml){
        $this->data = $dataHtml;

        $out = explode("table class=\"listEvent sro\">", $this->data);
       
       if(isset($out[1])){
           $output = explode("<table class=\"listEvent sro\">",$this->data);
           $output = explode("</table>",$output[1]);
           $output = str_replace("</td>","",$output[0]);
					$output = str_replace("</tr>","",$output);
					$output = str_replace("<strong>","",$output);
					$output = str_replace("</strong>","",$output);
					$output = str_replace("<tbody>","",$output);
                    $output = str_replace("</tbody>","",$output);
          $output = str_replace("<label style=\"text-transform:capitalize;\">","",$output);
					$output = str_replace("</label>","",$output);
					$output = str_replace("&nbsp;","",$output);
					$output = str_replace("<td class=\"sroDtEvent\" valign=\"top\">","",$output);
                    $output = explode("<tr>",$output);
                    
                    
                    //echo $output;     
                foreach($output as $texto){                      

                        $info   = explode("<td class=\"sroLbEvent\">",$texto);
						$dados  = explode("<br />",$info[0]);

						$dia   = trim($dados[0]);
						$hora  = trim(@$dados[1]);
						$local = trim(@$dados[2]);

						$dados = explode("<br />",@$info[1]);
						$acao  = trim($dados[0]);

						$exAction   = explode($acao."<br />",@$info[1]);
						$acrionMsg  = strip_tags(trim(preg_replace('/\s\s+/', ' ',$exAction[0])));

						if(""!=$dia){

							$exploDate = explode('/',$dia);
							$dia1 = $exploDate[2].'-'.$exploDate[1].'-'.$exploDate[0];
							$dia2 = date('Y-m-d');

							$diferenca = strtotime($dia2) - strtotime($dia1);
							//$dias = floor($diferenca / (60 * 60 * 24));

							//$change = utf8_encode("há {$dias} dias");


						    $novo_array[] = array(							
								  "date"=>$dia,
								  "hour"=>$hora,
								  "location"=>$local,
								  "action"=>utf8_encode($acao),
								  "message"=>utf8_encode($acrionMsg)
								  //"change"=>utf8_decode($change)
															  
							);
							
					  }
                    }
                    
                    $jsonObject = (object)$novo_array;
					header('Content-type: application/json');
					$json = json_encode($jsonObject);
					//$dadosJson = json_decode($json, true);
					//$this->getDataJson = $json;

					echo $json;					
										
       }else{
		   echo "O nosso sistema não possui dados sobre o objeto informado.!";		   
	   }	   
	   
	}
	 
}