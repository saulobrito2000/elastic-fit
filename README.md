@Rastreio de Objetos

Este é uma sistema de rastreio de objetos dos correios feito em PHP, nele se observa que fora feito por método GET. Ainda sim será atualizado para método POST e no modelo MVC, para organizar e separar o que back-end e o front-end, tornando um código limpo.

A seguir a descrição:

Em "index.php" está o formulário de pesquisa do objeto. Na busca, o código de rastreio é enviado via GET para o arquivo "send.php" que realiza três trabalhos e envia o codigo para um arquivo chamado "Trackback.php". O trackback se comunica com a api do correios que envia tudo em arquivo de texto.

O arquivo Trackback.php envia esse texto recebido para outro arquivo chamado "DataProcessing.php" que irá tratar do texto transformando em json.

No arquivo "send.php" a variável $url recebe os dados tratados e os envia por email os dados etapas por local e data e ainda enviar tudo isso em pdf.

O projeto quando realizado com um código mais limpo tem mais desempenho e organização, o método POST será utilizado para maior segurança implementado em MVC.