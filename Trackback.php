<?php
require 'DataProcessing.php';

$code = filter_input(INPUT_GET, 'obj', FILTER_DEFAULT);

if($code){
    $trackBack = array('Objetos' => $code);
    
    $lib = curl_init();
    
    curl_setopt($lib, CURLOPT_URL, 'https://www2.correios.com.br/sistemas/rastreamento/resultado_semcontent.cfm');
    curl_setopt($lib, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($lib, CURLOPT_POSTFIELDS, http_build_query($trackBack));
    $html = curl_exec($lib);
    curl_close($lib);
    
    $dataHtml = new DataProcessing();
    $dataHtml->convertToJson($html);
    //echo json_decode($dataHtml->getJson());
    
}else{
    echo "Campo vazio!";
}

       
