<?php

namespace App\Communication;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception as PHPMailerException;

class Email {

    const HOST = 'smtp.gmail.com';
    const USER = 'saulobrito2000@gmail.com';
    const PASS  = 'XXXXXXXX';
    const SECURE = 'tls';
    const PORT = 587; //465
    const CHARSET = 'UTF-8';

    
    const FROM_EMAIL = 'saulobrito2000@gmail.com';
    const FROM_NAME = 'Saulo Rios';

    private $error;

    public function getError(){
        return $this->error;
    }

    public function sendEmail($addresses, $subject, $body, $attachments = [], $ccs = [], $bccs = []){

        $this->error = '';

        $obMail = new PHPMailer(true);
        try{
            $obMail->isSMTP(true);
            $obMail->Host = self::HOST;
            $obMail->SMTPAuth = true;
            $obMail->Username = self::USER;
            $obMail->Password = self::PASS;
            $obMail->SMTPSecure = self::SECURE;
            $obMail->Port = self::PORT;
            $obMail->Charset = self::CHARSET;
            $obMail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $obMail->setFrom(self::FROM_EMAIL, self::FROM_NAME);

            //DESTINATÁRIOS
            $addresses = is_array($addresses) ? $addresses : [$addresses];
            foreach($addresses as $address){
                $obMail->addAddress($address);
            }

            //ANEXOS
            $attachments = is_array($attachments) ? $attachments : [$attachments];
            foreach($attachments as $attachment){
                $obMail->addAttachment($attachment);
            }

            //CC
            $ccs = is_array($ccs) ? $ccs : [$ccs];
            foreach($ccs as $cc){
                $obMail->addCC($cc);
            }

            //BCC
            $bccs = is_array($bccs) ? $bccs : [$bccs];
            foreach($bccs as $bcc){
                $obMail->addBCC($bcc);
            }

            //CONTEUDO DO E-MAIL
            $obMail->isHTML(true);
            $obMail->Subject = $subject;
            $obMail->Body = $body;

            return $obMail->send();

        }catch(PHPMailerException $e){
            $this->error = $e->getMessage();
            return false;
        }
    }
}