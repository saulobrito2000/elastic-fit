<?php
require __DIR__.'/vendor/autoload.php';
use Dompdf\Dompdf;

require_once('dompdf/autoload.inc.php');

$dompdf = new DOMPDF();

use \App\Communication\Email;

if(isset($_GET['obj'])){

    $obj = $_GET['obj'];
    $url = "http://localhost/elastic.fit/Trackback.php?obj={$obj}";    
    
}


    $address = 'saulobrito2000@gmail.com'; // E-mail do destinatário
    $assunto = '"TESTE" / Objeto de rastreio pelo código '.$obj.' - Por Saulo Rios :)';
    $subject = utf8_decode($assunto);
    

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

</head>
<body style="padding: 20px; margin: 20px;">

<?php


if (isset($url)) {
    $jsonFile = file_get_contents($url);
    $jsonStr = json_decode($jsonFile);        
    
    if ($jsonStr == '') {
        echo "Campo vazio, digite um código de rastreio";
    }else{
        echo $body = '<table class="table table-striped table-hover" style="width: 100%" id="mydatatable">';
        echo $body .= '<tbody>';

        $obEmail = new Email();
        
                  foreach ($jsonStr as $member) {                  
                     
                    $body .="<tr>";
                    $body .="<td>";
                    $body .= $member->date."<br>";
                    $body .= $member->hour."<br>";
                    $body .= $member->location;
                    $body .="</td>";
                    $body .="<td> ".utf8_decode($member->message)."</td>";
                    $body .="</tr>";	
                }
                echo $body;               
                
                $body .= '</tbody>';  
                $body .= '</table>';
                $body .= '<br><br>';
                $body .= utf8_decode('<b>Nome: Saulo José de Brito Rios<b><br>');
                $body .= '<b>Telefone: (98) 98101-3123<b>';
                $body .= '<br><br><br>';
                $body .= '<h3>Quais tecnologias usou?</h3>';
                $body .= utf8_decode('Utilizei o composer para melhor configurar as aplicações como PHPMailer e o DOMPDF');
                $body .= '<h3>Quais as maiores dificuldades?</h3>';
                $body .= utf8_decode('O meu maior desafio fora realizar o tratamento do array gerado pelo cURL no endereçamento do site correios, realizei o devido tratamento convertendo os dados para json e assim atribui-lo a uma tabela.
                tive algumas dificuldades na configuração do PHPMailer por conta de portas utilizáveis e segurança.');
                $body .= utf8_decode('<h3>O que não conseguiu fazer e o motivo?</h3>');
                $body .= utf8_decode('Não consegui adicionar algumas funcionalidades solicitados pelo enunciado problemas com o tempo a mim diponíveis, tais funcionalidades seriam a barra de status.
                foi uma experiencia e tanto para mim participar deste teste.');

                $attachment = __DIR__.'/Trackback.pdf';

                $dompdf->load_html($body);
                
                $dompdf->render();
                
                $output = $dompdf->output();
                    file_put_contents('Trackback.pdf', $output);
                
                $sucesso = $obEmail->sendEmail($address, $subject, $body, $attachment);
            }
        }

        

        echo $sucesso ? 'Mensagem enviada com sucesso!' : $obEmail->getError();
?>
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>

</html>